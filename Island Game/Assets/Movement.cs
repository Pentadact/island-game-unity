﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	private Rigidbody myRigidbody;

	// Use this for initialization
	void Start () {
		myRigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		//Movement
		float moveStrength = 10;
		if(Input.GetAxis("Horizontal") < 0) {
			myRigidbody.AddForce(transform.rotation * Vector3.left * moveStrength);
		}
		if(Input.GetAxis("Horizontal") > 0) {
			myRigidbody.AddForce(transform.rotation * Vector3.right * moveStrength);
		}
		if(Input.GetAxis("Vertical") < 0) {
			myRigidbody.AddForce(transform.rotation * Vector3.back * moveStrength);
		}
		if(Input.GetAxis("Vertical") > 0) {
			myRigidbody.AddForce(transform.rotation * Vector3.forward * moveStrength);
		}
	}
	void Update() {

		//Turning
		float turnSpeed = 190 * Time.deltaTime;
		if(Input.GetAxis("Mouse X") < 0) {
			transform.Rotate(Vector3.up * -turnSpeed);
		}
		if(Input.GetAxis("Mouse X") > 0) {
			transform.Rotate(Vector3.up * turnSpeed);
		}

	}
}
